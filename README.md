# FindFreeRTOS

Cmake script for FreeRTOS


## How to use


### Setup Environment


To use FindFreeRTOS, please download FreeRTOS Sources from [FreeRTOS official site](https://www.freertos.org/a00104.html).
And extract it and export there as FREERTOS_ROOT

ex.)
```
wget https://github.com/FreeRTOS/FreeRTOS/releases/download/202012.00/FreeRTOSv202012.00.zip
unzip FreeRTOSv202012.00.zip
export FREERTOS_ROOT=`pwd`/FreeRTOSv202012.00/
```

### Update your CMakeLists.txt


include the script

```
include(${SCRIPT_DIR}/FindFreeRTOS.cmake)
```

call FindFreeRTOS with arguments.


ex.)
Compiler is GCC, target is ARM Cortex-M4F, DYNAMIC_ALLOCATION is enabled.
```
FindFreeRTOS(COMPILER GCC PROCESSOR ARM_CM4F HEAP heap_1)
```


### Result Variables


After call the function above and if FreeRTOS was found, you can use variables to set to your cmake target.

#### FREERTOS_FOUND
Boolean value if FREERTOS was found. If did'not found, the variables listed below are never defined.

#### FREERTOS_INCLUDE_DIRS
which includes directory pathes for FreeRTOS include directories

#### FREERTOS_SOURCES
which includes file pathes for FreeRTOS Source.

#### FREERTOS_HEAP
which includes file pathes for FreeRTOS heap implementation. This variable is defined if argument "HEAP" is passed for `FindFreeRTOS`

#### FREERTOS_VERSION
FreeRTOS Version

